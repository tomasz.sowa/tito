#!/bin/sh

CURRENT_DIR=$(pwd)
GLOBAL_WORKING_DIR=$(realpath ${CURRENT_DIR}/..)

# ----------

# make sure the current directory is correct
if [ ! -d ${CURRENT_DIR}/.templates ] ; then
	echo "this script should be called by make: make clangd (one level up)"
	exit 1
fi

# change / to \/ in paths
CURRENT_DIR_FOR_SED=$(echo ${CURRENT_DIR} | sed 's/\//\\\//g')
GLOBAL_WORKING_DIR_FOR_SED=$(echo ${GLOBAL_WORKING_DIR} | sed 's/\//\\\//g')


if [ -f "${CURRENT_DIR}/.templates/.clangd" ] ; then
	if [ ! -f "${CURRENT_DIR}/.clangd" ] ; then
		cat "${CURRENT_DIR}/.templates/.clangd" | \
			sed -e "s/%%%CURRENT_DIR%%%/${CURRENT_DIR_FOR_SED}/g" \
				-e "s/%%%GLOBAL_WORKING_DIR%%%/${GLOBAL_WORKING_DIR_FOR_SED}/g" > \
			${CURRENT_DIR}/.clangd
			
		echo "added a ${CURRENT_DIR}/.clangd file"
	fi
fi


