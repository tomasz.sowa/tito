

export CXX
export CXXFLAGS
export AR


all: src


src: FORCE
	$(MAKE) -C src


clean: FORCE
	$(MAKE) -C src clean


depend: FORCE
	$(MAKE) -C src depend


clangd: FORCE
	@.templates/install_clangd.sh


qtcreator: clangd
	@.templates/install_qtcreator.sh


FORCE:

