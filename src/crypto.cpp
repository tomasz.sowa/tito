/*
 * This file is a part of Tito - a cryptography library
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2012-2022, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "crypto.h"
#include "utf8/utf8.h"
#include "misc.h"


namespace Tito
{


void Crypto::AssignString(const char * src, size_t len, std::wstring & dst, bool clear)
{
	if( clear )
		dst.clear();

	if( dst.capacity() < dst.size() + len )
		dst.reserve(dst.size() + len + 128);

	for(size_t i=0 ; i<len ; ++i )
		dst += static_cast<unsigned char>(src[i]);
}

void Crypto::AssignString(const wchar_t * src, size_t len, std::string & dst, bool clear)
{
	if( clear )
		dst.clear();

	if( dst.capacity() < dst.size() + len )
		dst.reserve(dst.size() + len + 128);

	for(size_t i=0 ; i<len ; ++i)
		dst += static_cast<char>(src[i]);
}


void Crypto::AssignString(const std::string & src, std::wstring & dst, bool clear)
{
	AssignString(src.c_str(), src.size(), dst, clear);
}

void Crypto::AssignString(const std::wstring & src, std::string & dst, bool clear)
{
	AssignString(src.c_str(), src.size(), dst, clear);
}





Crypto::Crypto()
{
	dummy_char = 0;
	plog = nullptr;
}



bool Crypto::SetAESKey(unsigned char * aes_key, int key_length)
{
	if( !aes.Key(aes_key, key_length) )
	{
		if( plog )
		{
			(*plog) << pt::Log::log2 << "Crypto: I cannot initialize the AES key" << pt::Log::logend;
		}

		return false;
	}
		
return true;
}



void Crypto::Crypt(const std::wstring & in, std::wstring & out)
{
	pt::wide_to_utf8(in, utf8_str);

	if( utf8_str.empty() )
		aes.Encode(&dummy_char, 0, aes_name, uint8_tab);
	else
		aes.Encode((const unsigned char*)utf8_str.c_str(), utf8_str.size(), aes_name, uint8_tab);
	
	base64_str.clear();

	if( !uint8_tab.empty() )
		base64.Encode((const char*)&uint8_tab[0], uint8_tab.size(), base64_str);
	
	AssignString(base64_str, out);
	Clear();
}


void Crypto::Crypt(const std::string & in, std::string & out)
{
	if( in.empty() )
		aes.Encode(&dummy_char, 0, aes_name, uint8_tab);
	else
		aes.Encode((const unsigned char*)&in[0], in.size(), aes_name, uint8_tab);

	out.clear();

	if( !uint8_tab.empty() )
		base64.Encode((const char*)&uint8_tab[0], uint8_tab.size(), out);

	Clear();
}


void Crypto::Crypt(const std::vector<char> & in, std::string & out)
{
	if( in.empty() )
		aes.Encode(&dummy_char, 0, aes_name, uint8_tab);
	else
		aes.Encode((const unsigned char*)&in[0], in.size(), aes_name, uint8_tab);

	out.clear();

	if( !uint8_tab.empty() )
		base64.Encode((const char*)&uint8_tab[0], uint8_tab.size(), out);

	Clear();
}



bool Crypto::Decrypt(const std::wstring & in, std::wstring & out)
{
	out.clear();

	if( in.empty() )
		return true;
	
	AssignString(in, base64_str);
	base64.Decode(base64_str, aes_str);

	if( aes.Decode((const unsigned char *)aes_str.c_str(), aes_str.size(), aes_tmp_name, uint8_tab) )
	{
		if( !uint8_tab.empty() )
			pt::utf8_to_wide((const char*)&uint8_tab[0], uint8_tab.size(), out);

		Clear();
		return true;
	}
	else
	{
		//log << "problem with AES decoding (skipping)" << logend;
	}

	Clear();

return false;
}


bool Crypto::Decrypt(const std::string & in, std::string & out)
{
	out.clear();

	if( in.empty() )
		return true;
	
	base64.Decode(in, aes_str);

	if( aes.Decode((const unsigned char *)aes_str.c_str(), aes_str.size(), aes_tmp_name, uint8_tab) )
	{
		out.resize(uint8_tab.size());

		for(size_t i=0 ; i<uint8_tab.size() ; ++i)
			out[i] = uint8_tab[i];

		Clear();
		return true;
	}
	else
	{
		//log << "problem with AES decoding (skipping)" << logend;
	}

	Clear();

return false;
}


bool Crypto::Decrypt(const std::string & in, std::vector<char> & out)
{
	out.clear();

	if( in.empty() )
		return true;
	
	base64.Decode(in, aes_str);

	if( aes.Decode((const unsigned char *)aes_str.c_str(), aes_str.size(), aes_tmp_name, uint8_tab) )
	{
		out.resize(uint8_tab.size());

		for(size_t i=0 ; i<uint8_tab.size() ; ++i)
			out[i] = uint8_tab[i];

		Clear();
		return true;
	}
	else
	{
		//log << "problem with AES decoding (skipping)" << logend;
	}

	Clear();

return false;
}



void Crypto::Clear()
{
	Clear(uint8_tab);
	Clear(aes_name);
	Clear(aes_tmp_name);
	Clear(utf8_str);
	Clear(base64_str);
	Clear(aes_str);
}

void Crypto::set_logger(pt::Log * plog)
{
	this->plog = plog;
	aes.set_logger(plog);
}



} // namespace Tito


