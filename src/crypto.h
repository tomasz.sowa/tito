/*
 * This file is a part of Tito - a cryptography library
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2012-2022, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef headerfile_tito_crypto
#define headerfile_tito_crypto

#include "aes.h"
#include "base64.h"
#include "log/log.h"


namespace Tito
{



class Crypto
{
public:

	Crypto();

	/*
		setting AES key (key length can be: 16, 24 or 32 bytes)
	*/
	bool SetAESKey(unsigned char * aes_key, int key_length);


	/*
		crypting AES256 and base64 then
		so the 'out' string is ready to save as a normal text
	*/
	void Crypt(const std::wstring & in,			std::wstring & out);
	void Crypt(const std::string & in,			std::string & out);
	void Crypt(const std::vector<char> & in,	std::string & out);


	/*
		decrypting base64 and AES256 then
		decrypt can return false if the control sums are incorrect
		in such a case out is empty
		if 'in' is empty the 'out' will be empty too
	*/
	bool Decrypt(const std::wstring & in,	std::wstring & out);
	bool Decrypt(const std::string & in,	std::string & out);
	bool Decrypt(const std::string & in,	std::vector<char> & out);


	/*
		clearing a string or a vector
		first it puts some characters and then calls clear() method
	*/
	template<class BufferType>
	void Clear(BufferType & str);


	/*
	 * pikotools logger
	 * default: null (no logging)
	 */
	void set_logger(pt::Log * plog);



private:

	// AES cryptography
	AES aes;

	// base64 encoding/decoding
	Base64 base64;

	// a table used with AES
	std::vector<AES::uint8> uint8_tab;

	// AES name (always empty)
	std::string aes_name;
	std::string aes_tmp_name;

	std::string utf8_str;
	std::string base64_str;
	std::string aes_str;

	// a dummy character used when an input string is empty
	unsigned char dummy_char;

	// logger, default null
	pt::Log * plog;


	void Clear();


	// temporarily fix for Visual Studio
	// crypto.obj : error LNK2001: unresolved external symbol "void __cdecl Tito::AssignString(char const *,unsigned int,class std::basic_string<wchar_t,struct std::char_traits<wchar_t>,class std::allocator<wchar_t> > &,bool)" (?AssignString@Tito@@YAXPBDIAAV?$basic_string@_WU?$char_traits@_W@std@@V?$allocator@_W@2@@std@@_N@Z)
	void AssignString(const char * src, size_t len, std::wstring & dst, bool clear = true);
	void AssignString(const wchar_t * src, size_t len, std::string & dst, bool clear = true);
	void AssignString(const std::string & src, std::wstring & dst, bool clear = true);
	void AssignString(const std::wstring & src, std::string & dst, bool clear = true);


};



template<class BufferType>
void Crypto::Clear(BufferType & str)
{
	for(size_t i=0 ; i<str.size() ; ++i)
		str[i] = 0x5c;

	str.clear();
}



} // namespace Tito



#endif


