/*
 * This file is a part of Tito - a cryptography library
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2018, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef headerfile_tito_base32
#define headerfile_tito_base32

#include <string>


namespace Tito
{


class Base32
{
public:

	Base32();

	// default '=';
	void SetPadding(char c);

	void Encode(const char * in, size_t len,	std::string & out);
	void Encode(const char * in,				std::string & out);
	void Encode(const std::string & in,			std::string & out);

	bool Decode(const char * in, size_t len,	std::string & out);
	bool Decode(const char * in,				std::string & out);
	bool Decode(const std::string & in,			std::string & out);



private:

	static const char * tabbase32;
	size_t tabbase32_len;
	char padding;
	unsigned int not_existing;

	void Convert5to8Pieces(const char * s, size_t len, unsigned int * tab8);
	void ConvertFrom8to5Pieces(const unsigned int * tab8, unsigned int * tab5);
	bool CharFromBase32(unsigned int from, unsigned int & to);
	bool Make8pieces(const char * s, unsigned int * tab8);
	void Save8PiecesToString(std::string & s, const unsigned int * tab8);
	void Save5PiecesToString(std::string & s, const unsigned int * tab5);

};



} // namespace Tito



#endif

