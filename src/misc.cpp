/*
 * This file is a part of Tito - a cryptography library
 * and is distributed under the 2-Clause BSD licence.
 * Author: Tomasz Sowa <t.sowa@ttmath.org>
 */

/* 
 * Copyright (c) 2012, Tomasz Sowa
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <fstream>
#include "misc.h"


namespace Tito
{



void AssignString(const char * src, size_t len, std::wstring & dst, bool clear)
{
	if( clear )
		dst.clear();

	if( dst.capacity() < dst.size() + len )
		dst.reserve(dst.size() + len + 128);

	for(size_t i=0 ; i<len ; ++i )
		dst += static_cast<unsigned char>(src[i]);
}



void AssignString(const char * src, std::wstring & dst, bool clear)
{
size_t len;

	for(len=0 ; src[len] ; ++len){}

	AssignString(src, len, dst, clear);
}


void AssignString(const std::string & src, std::wstring & dst, bool clear)
{
	AssignString(src.c_str(), src.size(), dst, clear);
}





void AssignString(const wchar_t * src, size_t len, std::string & dst, bool clear)
{
	if( clear )
		dst.clear();

	if( dst.capacity() < dst.size() + len )
		dst.reserve(dst.size() + len + 128);

	for(size_t i=0 ; i<len ; ++i)
		dst += static_cast<char>(src[i]);
}


void AssignString(const wchar_t * src, std::string & dst, bool clear)
{
size_t len;

	for(len=0 ; src[len] ; ++len){}

	AssignString(src, len, dst, clear);
}


void AssignString(const std::wstring & src, std::string & dst, bool clear)
{
	AssignString(src.c_str(), src.size(), dst, clear);
}



void AssignString(const char * src, size_t len, std::string & dst, bool clear)
{
	if( clear )
		dst.clear();

	// we suppose that append is smart enough and we don't have to use reserve()
	dst.append(src, len);
}



void AssignString(const char * src, std::string & dst, bool clear)
{
size_t len;

	for(len=0 ; src[len] ; ++len){}

	AssignString(src, len, dst, clear);
}




void AssignString(const std::string & src, std::string & dst, bool clear)
{
	if( clear )
		dst.clear();

	dst.append(src);
}


void AssignString(const wchar_t * src, size_t len, std::wstring & dst, bool clear)
{
	if( clear )
		dst.clear();

	// we suppose that append is smart enough and we don't have to use reserve()
	dst.append(src, len);
}


void AssignString(const wchar_t * src, std::wstring & dst, bool clear)
{
size_t len;

	for(len=0 ; src[len] ; ++len){}

	AssignString(src, len, dst, clear);
}



void AssignString(const std::wstring & src, std::wstring & dst, bool clear)
{
	if( clear )
		dst.clear();

	dst.append(src);
}




} // namespace Tito







